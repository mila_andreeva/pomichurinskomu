'use strict';
//bootstrap init
require('bootstrap');
//menu aside init
require('jquery.mmenu');

// var form = require('./libs/form.js');
var aside_menu = require('./libs/aside_menu.js');

require('jquery-ui/ui/widgets/tabs');
$('#tabs').tabs();

var magnificPopup = require('magnific-popup');
$('.js-popup').magnificPopup({
  type:'inline',
  midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
});

$('nav > a').bind("click", function(e){
	var anchor = $(this);
	$('html, body').stop().animate({
	scrollTop: $(anchor.attr('href')).offset().top
	}, 1000);
	e.preventDefault();
});


var slick = require('slick-carousel');
// $('.sliderSlick__container').slick({
//   dots: true,
//   arrows: false,
//   dotsClass: 'sliderSlick__dots',
//   adaptiveHeight: false

// });


$(window).scroll(function() {
    var windowWidth = $(window).width();
    if ($(this).scrollTop() > 150 && windowWidth > 767) {
       $('#menu').slideUp();
       $('.header').css({'background-color' : 'rgba(255,255,255,1)'});
       $('.header').addClass('header-scroll')
       $('.color').css({'color': "#656556"});
       $('#name-top').css({'display': 'block'});
    }
    if ($(this).scrollTop() < 150 && windowWidth > 767) {
        $('#menu').slideDown();
        $('.header').css({'background-color' : 'rgba(0,0,0,0.5)'});
        $('.header').removeClass('header-scroll')
        $('.color').css({'color': '#FFF'});
        $('#name-top').css({'display': 'none'});
    }
});


// $(window).scroll(function() {
//     if ($(this).scrollTop() < 100 ) {
//       $('#name-top').slideUp();
//       $('#name-foto').slideDown();
//     }
//     if ($(this).scrollTop() > 100 ) {
//       $('#name-foto').slideUp();
//       $('#name-top').slideDown();
//     }

// });


///toastr init
var toastr = require('toastr');

//popup&form send
var magnificPopup = require('magnific-popup');
$('.js-popup').magnificPopup();

$( "form" ).submit(function( e ) {

  e.preventDefault();

  var $form = $( this ),
    name    = $form.find( "input[name='name']" ).val(),
    email   = $form.find( "input[name='email']" ).val(),
    phone   = $form.find( "input[name='phone']" ).val(),
    message = $form.find( "textarea[name='message']" ).val(),

    // url     = $form.attr( "action" );
    url = "../mail.php";

  var data = {
    form: $form.attr('title'),
    name: name

  };

  if(email) {
    data.email = email;
  }

  if(phone) {
    data.phone = phone;
  }

  if(message) {
    data.message = message;
  }

  var posting = $.post( url, data);

  posting
    .done(function() {
      toastr.success('Ваша заявка успешно отправленна');
      var inputClean = $form.find("input");
      inputClean.val('');
      })
    .fail(function() {
      console.log('test');
      var inputClean = $form.find("input");
      inputClean.val('');
      toastr.error('Ошибка отправки');
      console.log('error');
    })
    .always(function() {
      $.magnificPopup.close();
    });
    console.log(data);
});

// mapPrice();
//commonjs
// var tabs = require('tabs');
//
// //or directly include the script and 'tabs' will be global
//
// // make it tabbable!
// var container=document.querySelector('.tab-container')
// tabs(container);
// $('.slider-photo-gallery').slick({
//   centerMode: true,
//   centerPadding: '60px',
//   slidesToShow: 4,
//   responsive: [
//     {
//       breakpoint: 768,
//       settings: {
//         arrows: false,
//         centerMode: true,
//         centerPadding: '40px',
//         slidesToShow: 2
//       }
//     },
//     {
//       breakpoint: 480,
//       settings: {
//         arrows: false,
//         centerMode: true,
//         centerPadding: '40px',
//         slidesToShow: 1
//       }
//     }
//   ]
// });

// $('.slider-photo-gallery').slick({
//   slidesToShow: 1,
//   fade: true,
//   slidesToScroll: 1,
//   arrows: false,
//   fade: true,
//   asNavFor: '.slider-photo-gallery__nav'
// });
// $('.slider-photo-gallery__nav').slick({
//   slidesToShow: 4,
//   slidesToScroll: 1,
//   asNavFor: '.slider-photo-gallery',
//   centerMode: true,
//   focusOnSelect: true,
//   responsive:[
//     {
//       breakpoint: 768,
//       settings: {
//         arrows: false,
//         centerMode: true,
//         centerPadding: '10px',
//         slidesToShow: 1
//       }
//     },
//     {
//       breakpoint: 200,
//       settings: {
//         settings: "unslick",
//         arrows: false,
//         centerMode: true,
//         centerPadding: '2px',
//         slidesToShow: 1
//       }
//     }
//   ]
// });
$('.multiple-items').slick({
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  centerMode: true,
  variableWidth: true
});
// $('').slick({
//   infinite: true,
//   slidesToShow: 3,
//   slidesToScroll: 3,
  
// });   

$('.center').slick({
  centerMode: true,
  centerPadding: '60px',
  slidesToShow: 3,
  prevArrow: '<div class="slider-prev"></div>',
  nextArrow: '<div class="slider-next"></div>',
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});
var Revealator = typeof Revealator !== 'undefined' ? Revealator : {};

// $(function () {
//   Revealator = $.extend({}, {
//     timer:           null,
//     busy:            false,
//     scroll_padding:  0,
//     effects_padding: 0,
//     refresh:         function () {}
//   }, typeof Revealator !== 'undefined' ? Revealator : {});

//   Revealator.refresh = function () {
//     var $window = $(window);
//     var $document = $(document);
//     var $body = $(document.body);
//     var i = 0;
//     var window_top = Revealator.effects_padding;
//     var window_bottom = $window.height() - Revealator.effects_padding;
//     var document_top = Revealator.scroll_padding;
//     var document_bottom = $document.height() - Revealator.scroll_padding;

//     if ($window.scrollTop() === 0) {
//       if (!$body.hasClass('at-top')) {
//         $body.addClass('at-top').removeClass('at-bottom').removeClass('near-top').removeClass('near-bottom');
//       }
//     } else if ($window.scrollTop() + $window.height() === $document.height()) {
//       if (!$body.hasClass('at-bottom')) {
//         $body.addClass('at-bottom').removeClass('at-top').removeClass('near-top').removeClass('near-bottom');
//       }
//     } else if ($window.scrollTop() <= document_top) {
//       if (!$body.hasClass('near-top')) {
//         $body.addClass('near-top').removeClass('near-bottom').removeClass('at-top').removeClass('at-bottom');
//       }
//     } else if ($window.scrollTop() + $window.height() >= document_bottom) {
//       if (!$body.hasClass('near-bottom')) {
//         $body.addClass('near-bottom').removeClass('near-top').removeClass('at-top').removeClass('at-bottom');
//       }
//     } else {
//       if ($body.hasClass('at-top') || $body.hasClass('at-bottom') || $body.hasClass('near-top') || $body.hasClass('near-bottom')) {
//         $body.removeClass('at-top').removeClass('at-bottom').removeClass('near-top').removeClass('near-bottom');
//       }
//     }

//     $('*[class*="revealator"]').each(function () {
//       i++;
//       var element = this;
//       var $element = $(element);
//       var element_bounding = element.getBoundingClientRect();

//       var position_class = undefined;
//       if (element_bounding.top > window_bottom && element_bounding.bottom > window_bottom) {
//         position_class = 'revealator-below';
//       } else if (element_bounding.top < window_bottom && element_bounding.bottom > window_bottom) {
//         position_class = 'revealator-partially-below'
//       } else if (element_bounding.top < window_top && element_bounding.bottom > window_top) {
//         position_class = 'revealator-partially-above'
//       } else if (element_bounding.top < window_top && element_bounding.bottom < window_top) {
//         position_class = 'revealator-above';
//       } else {
//         position_class = 'revealator-within';
//       }

//       if ($element.hasClass('revealator-load') && !$element.hasClass('revealator-within')) {
//         $element.removeClass('revealator-below revealator-partially-below revealator-within revealator-partially-above revealator-above');
//         $element.addClass('revealator-within');
//       }

//       if (!$element.hasClass(position_class) && !$element.hasClass('revealator-load')) {
//         if ($element.hasClass('revealator-once')) {
//           if (!$element.hasClass('revealator-within')) {
//             $element.removeClass('revealator-below revealator-partially-below revealator-within revealator-partially-above revealator-above');
//             $element.addClass(position_class);
//           }
//           if ($element.hasClass('revealator-partially-above') || $element.hasClass('revealator-above')) {
//             $element.addClass('revealator-within');
//           }
//         } else {
//           $element.removeClass('revealator-below revealator-partially-below revealator-within revealator-partially-above revealator-above');
//           $element.addClass(position_class);
//         }
//       }
//     });
//   };

//   $(window).bind('scroll resize load ready', function () {
//     if (!Revealator.busy) {
//       Revealator.busy = true;
//       setTimeout(function () {
//         Revealator.busy = false;
//         Revealator.refresh();
//       }, 150);
//     }
//   });
// });

var bouncejs = require('bounce.js');

function testFunc(name){
  var bounce = new bouncejs();
  // debugger;
  bounce
    .translate({
      from: { x: -300, y: 0 },
      to: { x: 0, y: 0 },
      duration: 600,
      stiffness: 4
    })
    .scale({
      from: { x: 1, y: 1 },
      to: { x: 0.1, y: 2.3 },
      easing: "sway",
      duration: 800,
      delay: 65,
      stiffness: 2
    })
    .scale({
      from: { x: 1, y: 1},
      to: { x: 5, y: 1 },
      easing: "sway",
      duration: 300,
      delay: 30,
    })
    .applyTo(name);
}
$(window).scroll(function(e) {
  // console.log("hui");
  var myScroll = $(this).scrollTop();
  // console.log(myScroll);
  var myElement = $('.testBlock');
  // debugger;
  if (myElement.length > 0){
    if ($(this).scrollTop() > myElement.offset().top - $(window).height() && !myElement.hasClass('animating') ) {
        // debugger;
        console.log("hui 2");
        myElement.addClass('animating');
        testFunc($('.testBlock'));
    }
  }
});


ymaps.ready(startMap);
var myMap,
    myPlacemark;

function startMap(){
  myMap = new ymaps.Map ("map", {
      center: [55.744460, 37.613018],
      zoom: 16
  });

  if ( window.innerWidth < 650 ){
    myMap.behaviors.disable('drag');
  }
  myMap.behaviors.disable('scrollZoom');
  myMap.behaviors.disable('multiTouch');
  myPlacemark = new ymaps.Placemark([55.744460, 37.613018], { hintContent: 'Москва ул.Серафимовича д.2', balloonContent: 'Москва ул.Серафимовича д.2' });
  myMap
    .geoObjects
    .add(myPlacemark);
}

$('.center').magnificPopup({
  delegate: 'a',
  type: 'image',
  closeOnContentClick: false,
  closeBtnInside: false,
  mainClass: 'mfp-with-zoom mfp-img-mobile',
  image: {
    verticalFit: true,
    titleSrc: function(item) {
      return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
    }
  },
  gallery: {
    enabled: true
  },
  zoom: {
    enabled: true,
    duration: 300, // don't foget to change the duration also in CSS
    opener: function(element) {
      return element.find('img');
    }
  }
    
  
});